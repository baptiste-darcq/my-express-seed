var express = require('express'),
  app = express();

app.set('views', __dirname + '/views');

app.use(express.static(__dirname + '/../front/dist/'+process.env.FRONT_FOLDER+'/'));

var cities = require('./routes/cities.js');
app.use('/cities', cities);

module.exports = app;
