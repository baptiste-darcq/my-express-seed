var express = require('express'),
  redis = require('redis'),
  bodyParser = require('body-parser'),
  urlencoded = bodyParser.urlencoded({ extended: false });

// REDIS connection
if (process.env.REDISTOGO_URL) {
  var rtg   = require("url").parse(process.env.REDISTOGO_URL);
  var db = redis.createClient(rtg.port, rtg.hostname);
  db.auth(rtg.auth.split(":")[1]);
} else {
  var db = redis.createClient();
  db.select((process.env.NODE_ENV || 'development').length);
}
// REDIS connection

var router = express.Router();

router.route('/')
  .get(function(request, response){
    db.hkeys('cities', function (error, names) {
      if (error) { throw error; }

      response.status(200).json(names);
    });
  })
  .post(urlencoded, function(request, response){
    var newCity = request.body;
    if (!newCity.name || !newCity.description) {
      response.sendStatus(400);
      return false;
    }
    db.hset('cities', newCity.name, newCity.description, function (error) {
      if (error) { throw error; }

      response.status(201).json(newCity.name);
    });
  });

router.route('/:name')
  .delete(function(request, response) {
    var city = request.params.name;
    db.hdel('cities', city, function (error) {
      if (error) { throw error; }

      response.sendStatus(204);
    });
  })
  .get(function(request, response) {
    db.hget('cities', request.params.name, function(error, description){
      response.render('show.ejs',
        { city:
          { name: request.params.name, description: description }
        }
      );
    });
  });

module.exports = router;
