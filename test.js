var request = require('supertest'),
  app = require('./app'),
  redis = require('redis'),
  db = redis.createClient();

db.select('test'.length);
db.flushdb();

describe('Requests to the root path', function(){

  it('Return 200 status code', function(done){
    request(app)
      .get('/')
      .expect(200, done);
  });
  it('Returns HTML format', function (done) {
    request(app)
      .get('/')
      .expect('Content-Type', /html/, done);
  });
  it('Returns an index file with Cities', function (done) {
    request(app)
      .get('/')
      .expect(/cities/i, done);
  });

});

describe('Listing cities on /cities', function () {

  it('Returns a 200 status code', function (done) {
    request(app)
      .get('/cities')
      .expect(200, done);
  });

  it('Returns JSON format', function (done) {
    request(app)
      .get('/cities')
      .expect('Content-type', /json/, done);
  });

  it('Returns initial cities', function (done) {
    request(app)
      .get('/cities')
      .expect(JSON.stringify([]), done);
  });

});

describe('Creating new cities', function () {

  it('Returns a 201 status code', function (done) {
    request(app)
      .post('/cities')
      .send('name=Springfield&description=Where+the+simpsons+live')
      .expect(201, done);
  });

  it('Returns the city name', function (done) {
    request(app)
      .post('/cities')
      .send('name=Springfield&description=Where+the+simpsons+live')
      .expect(/springfield/i, done);
  });

  it('Validates city name and description', function (done) {
    request(app)
      .post('/cities')
      .send('name=&description=')
      .expect(400, done);
  });

});

describe('Delete Cities', function () {

  before(function () {
    db.hset('cities', 'Banana', 'the tastiest fruit');
  });

  after(function () {
    db.flushdb();
  })

  it('Returns a 204 status code', function (done) {
    request(app)
      .delete('/cities/Banana')
      .expect(204, done);
  });

});

describe('Show city info', function () {

  before(function () {
    db.hset('cities', 'Banana', 'yellow fruit');
  });

  after(function () {
    db.flushdb();
  });

  it('Returns a 200 status code', function (done) {
    request(app)
      .get('/cities/Banana')
      .expect(200, done);
  });

  it('Returns HTML format', function (done) {
    request(app)
      .get('/cities/Banana')
      .expect('Content-Type', /html/, done);
  });

  it('Returns information for given city', function (done) {
    request(app)
      .get('/cities/Banana')
      .expect(/yellow/, done);
  });

});
